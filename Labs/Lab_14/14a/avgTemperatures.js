"use strict";
var weatherIconLink =
  "https://www.shareicon.net/data/128x128/2015/09/07/97237_cloud_512x512.png";
var precipIconLink = "https://d30y9cdsu7xlg0.cloudfront.net/png/135632-200.png";

var wx_data = [
  {
    day: "TUE",
    date: "NOV 20",
    hi: 55,
    lo: 54,
    icon: weatherIconLink,
    condition: " Rain",
    icon2: precipIconLink,
    precip: "100%",
    wind: "ESE 8 mph",
    humidity: "92%"
  },
  {
    day: "WED",
    date: "NOV 21",
    hi: 61,
    lo: 54,
    icon: weatherIconLink,
    condition: " Rain",
    icon2: precipIconLink,
    precip: "70%",
    wind: "SE 11 mph",
    humidity: "84%"
  },
  {
    day: "THU",
    date: "NOV 22",
    hi: 55,
    lo: 46,
    icon: weatherIconLink,
    condition: " AM Rain",
    icon2: precipIconLink,
    precip: "90%",
    wind: "SSW 15 mph",
    humidity: "84%"
  },
  {
    day: "FRI",
    date: "NOV 23",
    hi: 51,
    lo: 42,
    icon: weatherIconLink,
    condition: " AM Showers",
    icon2: precipIconLink,
    precip: "50%",
    wind: "SSE 9 mph",
    humidity: "80%"
  },
  {
    day: "SAT",
    date: "NOV 24",
    hi: 50,
    lo: 46,
    icon: weatherIconLink,
    condition: " Rain",
    icon2: precipIconLink,
    precip: "90%",
    wind: "ESE 5 mph",
    humidity: "81%"
  }
];

var hiTemp = [
  wx_data[0]["hi"],
  wx_data[1]["hi"],
  wx_data[2]["hi"],
  wx_data[3]["hi"],
  wx_data[4]["hi"]
];
var loTemp = [
  wx_data[0]["lo"],
  wx_data[1]["lo"],
  wx_data[2]["lo"],
  wx_data[3]["lo"],
  wx_data[4]["lo"]
];

function getAvg(total, num) {
  return total + num;
}

var hiAvg = hiTemp.reduce(getAvg) / hiTemp.length;
var loAvg = loTemp.reduce(getAvg) / loTemp.length;

var el = document.getElementById("weatherTable");
var i = 0;
for (i = 0; i < 5; i++) {
  el.innerHTML +=
    "<tr><td>" +
    wx_data[i]["day"] +
    "</td><td>" +
    wx_data[i]["date"] +
    "</td><td>" +
    wx_data[i]["hi"] +
    "&#8457 / " +
    wx_data[i]["lo"] +
    "&#8457" +
    "</td><td>" +
    '<img width=25 src="' +
    wx_data[i]["icon"] +
    '">' +
    wx_data[i]["condition"] +
    "</td><td>" +
    '<img width=25 src="' +
    wx_data[i]["icon2"] +
    '">' +
    wx_data[i]["precip"] +
    "</td><td>" +
    wx_data[i]["wind"] +
    "</td><td>" +
    wx_data[i]["humidity"] +
    "</td></tr>";
}

var el2 = document.getElementById("averageTable");

el2.innerHTML +=
  "<tr><td>" +
  hiAvg +
  " &#8457" +
  "</td><td>" +
  "Low: " +
  loAvg +
  " &#8457" +
  "</td></tr>";

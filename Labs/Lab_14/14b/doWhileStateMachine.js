function stateMachine() {
  var state = "IDLE";
  var cmd = "";
  var options = "OPTION: run, exit, quit";
  do {
    switch (state) {
      case "IDLE":
        {
          if (cmd === "run") {
            /*refreshPage();*/
            state = "S1";
            options = "OPTIONS: next, prev, skip, exit, quit";
          }
        }
        break;
      case "S1":
        {
          if (cmd === "next") {
            state = "S2";
            options = "OPTIONS: next, exit, quit";
          } else if (cmd === "skip") {
            state = "S3";
            options = "OPTIONS: next, home, exit, quit";
          } else if (cmd === "prev") {
            state = "S4";
            options = "OPTIONS: next, exit, quit";
          }
        }
        break;
      case "S2":
        {
          if (cmd === "next") {
            state = "S3";
            options = "OPTIONS: next, home, exit, quit";
          }
        }
        break;

      case "S3":
        {
          if (cmd === "next") {
            state = "S4";
            options = "OPTIONS: next, exit, quit";
          } else if (cmd === "home") {
            state = "IDLE";
            options = "OPTION: run, exit, quit";
          }
        }
        break;
      case "S4":
        {
          if (cmd === "next") {
            state = "S1";
            options = "OPTIONS: next, prev, skip, exit, quit";
          }
        }
        break;
    }
    cmd = prompt("Enter a command:" + state, options);
  } while (cmd != "exit" && cmd != "quit");
}

/*function refreshPage() {
  window.onunload = refreshParent();
  function refreshParent() {
    window.opener.location.reload();
  }
}*/

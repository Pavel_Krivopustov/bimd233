$(document).ready(function() {
  $("li").css("id", "uw");
  const states = ["idle", "gather", "process"];
  const stateIdle = 0;
  const stateGather = 1;
  const stateProcess = 2;
  const enter = 13;
  var state = states[0];
  var words = new Array();
  var ndx = 0;
  var curText = "";

  $("ul").on("mouseover", "li", function() {
    console.log("x:" + $(this).text());
    $(this).attr("id", "uw");
  });

  $("ul").on("mouseleave", "li", function() {
    $(this).attr("id", "uw-gold");
  });

  // reset button click
  $("button").on("click", function(e) {
    while(words.length > 0){
			var el = words.shift();
			el.parentNode.removeChild(el);
		}
  });

  // keypress
  $("input").on("keypress", function(e) {
    var code = e.which;
    var char = String.fromCharCode(code);
    console.log("key:" + code + "\tstate:" + state + "\tchar:" + char);
    if(state === states[stateGather] && $("input").val().length > 0 && code === enter)
      {
        state = states[stateProcess];
      } else if (state === states[stateGather] && $("input").val().length === 0 && code === enter) {
        state = states[stateIdle];
      }
    
    switch (state) {
      // idle
      case "idle":
        if(code !== enter)
          {
            state = states[stateGather];
          }
        break;

      // gather
      case "gather":
        break;

      // process
      case "process":
       
				var el = document.createElement('li');
				el.classList.add('list-group-item');
				el.id = "uw-gold";
				el.innerText = $('input').val();
				$('.list-group').append(el)
				words.push(el)
				$('input').val("");
				state = states[stateIdle];
        break;

      default:
        break;
    }
  });
});

var txt;
var charCode;

function car() {
    var carIdentifier = prompt("Please enter your car name:", "");
    if (carIdentifier == null || carIdentifier == "") {
        txt = "Car name not specified.";
    } else {
        txt = " for: " + carIdentifier;
    }
    document.getElementById("carName").innerHTML = txt;
    date();
}

function date() {
    var dateEntered = prompt("Please enter today's date (mm/dd/yyyy):", "");
    
    for(var i = 0; i < dateEntered.length; i++)
    {
        charCode = dateEntered.charCodeAt(i);
        if (charCode != 46 && charCode > 31 && (charCode < 47 || charCode > 57))
        {
           date();
        }
    }
      
    if (dateEntered == null || dateEntered == "") {
        date();
    } else {
        txt = "Prepared on: " + dateEntered;
    }
    document.getElementById("date").innerHTML = txt;
    deduciton();
}

function deduciton() {
    var milesDriven = prompt("Please enter miles driven:", "")
    if (milesDriven == null || milesDriven == "") {
        deduciton();
    }
    
    for(var i = 0; i < milesDriven.length; i++)
    {
        charCode = milesDriven.charCodeAt(i);
        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
        {
           deduciton();
        }
    }
    var totalDeduction = (milesDriven * .57)
    txt = "$" + totalDeduction.toFixed(2);
    document.getElementById("totalDeduction").innerHTML = txt;
}
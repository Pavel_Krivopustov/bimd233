"use strict";

function Flight(
  airline,
  number,
  origin,
  destination,
  departureTime,
  arrivalTime,
  arrivalGate,
  duration
) {
  this.airline = airline;
  this.number = number;
  this.origin = origin;
  this.destination = destination;
  this.departureTime = departureTime;
  this.arrivalTime = arrivalTime;
  this.arrivalGate = arrivalGate;
  this.duration = function() {
    var arrDate = new Date(arrivalTime);
    var depDate = new Date(departureTime);
    var diff = arrDate.getTime() - depDate.getTime();
    return msToTime(diff);
  };
}

function msToTime(duration) {
  var milliseconds = parseInt((duration % 1000) / 100),
    seconds = parseInt((duration / 1000) % 60),
    minutes = parseInt((duration / (1000 * 60)) % 60),
    hours = parseInt((duration / (1000 * 60 * 60)) % 24);
  hours = hours < 10 ? "0" + hours : hours;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  seconds = seconds < 10 ? "0" + seconds : seconds;
  return hours + ":" + minutes + ":" + seconds;
}

var flight0 = new Flight(
  "Delta",
  "DAL118",
  "Boston, MA",
  "Paris, FRANCE",
  "Wed Nov 15 07:03 pm GMT-4",
  "Wed Nov 16 08:02 am GMT+2",
  "TERMINAL 2E"
);
var flight1 = new Flight(
  "United",
  "UAL1118",
  "Cancun, MEXICO",
  "San Francisco, CA",
  "Wed Nov 15 05:45 pm GMT-1",
  "Wed Nov 15 08:45 pm GMT",
  "GATE G95"
);
var flight2 = new Flight(
  "American Airlines",
  "AAL1147",
  "Miami, FL",
  "Los Angeles, CA",
  "Wed Nov 15 09:16 pm EST",
  "Wed Nov 16 12:07 am PST",
  "GATE 47A"
);
var flight3 = new Flight(
  "Delta",
  "DAL1072",
  "Atlanta, GA",
  "Pittsburg, PA",
  "Wed Nov 15 10:26 pm EST",
  "Wed Nov 16 12:03 am EST",
  "GATE D76"
);
var flight4 = new Flight(
  "United",
  "UAL1116",
  "Newark, NJ",
  "San Francisco, CA",
  "Wed Nov 15 09:47 pm EST",
  "Wed Nov 16 01:34 am PST",
  "GATE 85"
);

var flights = [flight0,flight1,flight2,flight3,flight4];

var el = document.getElementById("flightTable");
var i = 0;
for (i = 0; i < 5; i++) {
  el.innerHTML +=
    "<tr><td>" +
    flights[i]["airline"] +
    "</td><td>" +
    flights[i]["number"] +
    "</td><td>" +
    flights[i]["origin"] +
    "</td><td>" +
    flights[i]["destination"] +
    "</td><td>" +
    flights[i]["departureTime"] +
    "</td><td>" +
    flights[i]["arrivalTime"] +
    "</td><td>" +
    flights[i]["arrivalGate"] +
    "</td><td>" +
    flights[i].duration() +
    "</td></tr>";
}
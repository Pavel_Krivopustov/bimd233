// Written By: Pavel Krivopustov
// Date: 02/24/2018
// Description:

function setup() {
  // Create a canvas to fill the entire browser
  createCanvas(window.innerWidth, window.innerHeight);
  // buttonSun.mousePressed();
  //noLoop();
  ellipse(window.innerWidth / 2, window.innerHeight / 2, window.innerHeight * .8, window.innerHeight * .8);
}

function preload() {

}


function draw() {
  //print(Math.cos(mouseX));
  //var degrees = Math.cos(mouseX);
  //degrees = convertToDegrees(degrees);
  var centerX = window.innerWidth / 2;
  var centerY = window.innerHeight / 2;
  degrees = Math.atan2(mouseY - centerY, mouseX - centerX);
  degrees = convertToDegrees(degrees);
  // print(degrees);

  let mapped = map(degrees, 0, 360, 0, 24);
  let time;
  if (mapped > 18) {
    time = mapped - 18;
  } else {
    time = mapped + 6;    
  }

  print("mapped: " + mapped + "time: " + time);

}

function convertToDegrees(radians)
  {
    var toDegrees = radians*(180/Math.PI);
    if(toDegrees < 0) {
      toDegrees = 360 - Math.abs(toDegrees);
    }
    return toDegrees;
  }
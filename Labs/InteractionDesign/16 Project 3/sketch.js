// Written By: Pavel Krivopustov
// Date Started: 02/24/2018
// Last Updated: 03/08/2018
// Description: This project visually demonstrates DUI offenses based on time of day.
//              By selecting the day of the week, spikes with corresponding color
//              to the button color appear around the clock. Multiple days may be selected
//              for comparison of DUI's on different days of the week. By approximately hovering
//              over the spikes around the clock, more information about the DUI accident will
//              be displayed on the tan panel to the right.

var crimeData; // initially stores the retrieved data

var buttonYCoor = 20; // how far the buttons are positioned from above
// colors to represent the buttons and the spikes
var sunColor = "rgb(248, 255, 27)";
var monColor = "rgb(167, 213, 78)";
var tueColor = "rgb(28, 172, 87)";
var wedColor = "rgb(37, 116, 191)";
var thuColor = "rgb(123, 76, 156)";
var friColor = "rgb(255, 0, 0)";
var satColor = "rgb(244, 136, 63)";
var docColor = "rgb(223, 207, 181)";
var theImage, theImage2; // images that will be loaded
var clockCenterX, clockCenterY; // coordinates denoting the center of the clock
var crimeInfo = ""; // string that will hold the DUI accident report
var crimeInfoOriginal =
    "Select Day of the Week \nand Hover Over the Data\nto See More Details";

var currentDays = []; // array that holds which days to display based on buttons selected
var dataLines = []; // stores all of the DUI cases in one place
var filteredLines = []; // stores DUI cases from dataLines based on the days selected

var radius = 100; // default radius for images
var btnSize = 120; // button length

function setup() {
  // Create a canvas to fill the entire browser
  createCanvas(window.innerWidth, window.innerHeight);
  // define new size to be able to fit the image on canvas
  imageSize = height / 1.5;
  // draw the two images
  image(
    theImage,
    (window.innerWidth - imageSize) / 5.0,
    window.innerHeight / 2 - imageSize / 2,
    imageSize,
    imageSize
  );
  image(
    theImage2,
    (window.innerWidth - imageSize) / 1.1,
    window.innerHeight / 2 - imageSize / 2,
    imageSize,
    imageSize
  );
  // calculate the image radius
  radius = theImage.height / 1.5 / 2.0;
  // calculate the center of the clock image
  clockCenterX = (window.innerWidth - imageSize) / 5.0 + imageSize / 2;
  clockCenterY = window.innerHeight / 2;
  // set up the text setting and display the message in the panel
  fill("black");
  textSize(27);
  text(
    crimeInfoOriginal,
    80 + (window.innerWidth - imageSize) / 1.1,
    15 + window.innerHeight / 2 - imageSize / 2,
    imageSize - 180,
    imageSize
  );
}

function preload() {
  // load images from the specified folder
  theImage = loadImage("resources/clock.png");
  theImage2 = loadImage("resources/doc.png");
  // fetch the data from the JSON file using the API
  $.ajax({
    url: "https://moto.data.socrata.com/resource/4h35-4mtu.json",
    type: "GET",
    data: {
      $limit: 30000,
      $$app_token: "DuaakxC45Z6cqANMR8SNOy4Bf"
    }
    // when the function loads the data
  }).done(function(data) {
    //alert("Retrieved " + data.length + " records from the dataset!");
    crimeData = data;
    // filter the results based on DUI cases and ensure that unspecified times are not included
    processData(crimeData);

    // add buttons to the top of the screen
    buttonSun = createButton("SUNDAY");
    buttonSun.position((width / 8) - btnSize / 2.0, buttonYCoor);
    buttonSun.style("background-color", "black");
    buttonSun.style("color", "white");
    // sets the length size of the button to maintain consistency in size
    buttonSun.size(btnSize);

    buttonMon = createButton("MONDAY");
    buttonMon.position((2 * (width / 8)) - btnSize / 2.0, buttonYCoor);
    buttonMon.style("background-color", "black");
    buttonMon.style("color", "white");
    buttonMon.size(btnSize);

    buttonTue = createButton("TUESDAY");
    buttonTue.position((3 * (width / 8) - btnSize / 2.0), buttonYCoor);
    buttonTue.style("background-color", "black");
    buttonTue.style("color", "white");
    buttonTue.size(btnSize);

    buttonWed = createButton("WEDNESDAY");
    buttonWed.position((4 * (width / 8) - btnSize / 2.0), buttonYCoor);
    buttonWed.style("background-color", "black");
    buttonWed.style("color", "white");
    buttonWed.size(btnSize);

    buttonThu = createButton("THURSDAY");
    buttonThu.position((5 * (width / 8)) - btnSize / 2.0, buttonYCoor);
    buttonThu.style("background-color", "black");
    buttonThu.style("color", "white");
    buttonThu.size(btnSize);

    buttonFri = createButton("FRIDAY");
    buttonFri.position((6 * (width / 8) - btnSize / 2.0), buttonYCoor);
    buttonFri.style("background-color", "black");
    buttonFri.style("color", "white");
    buttonFri.size(btnSize);

    buttonSat = createButton("SATURDAY");
    buttonSat.position((7 * (width / 8)) - btnSize / 2.0, buttonYCoor);
    buttonSat.style("background-color", "black");
    buttonSat.style("color", "white");
    buttonSat.size(btnSize);

    // after the button is pressed, change the color and draw the elements
    buttonSun.mousePressed(function(e) {
      //console.log(e);
      // change the color of the button depending if it was clicked or not
      if (buttonSun.elt.style.backgroundColor == sunColor) {
        // when disabled, the button is black
        buttonSun.style("background-color", "black");
        buttonSun.style("color", "white");
        // if the button is deselected, remove the day to display from the currentDays array
        removeFromCurrentDaysArray("sunday");
      } else {
        // when selected, change to the color of the day
        buttonSun.style("background-color", sunColor);
        buttonSun.style("color", "black");
        // push the day to the currentDays array
        currentDays.push("sunday");
        // filter and then display the data on the clock
        filterByDay();
      }
    });

    buttonMon.mousePressed(function(e) {
      if (buttonMon.elt.style.backgroundColor == monColor) {
        buttonMon.style("background-color", "black");
        buttonMon.style("color", "white");
        removeFromCurrentDaysArray("monday");
      } else {
        buttonMon.style("background-color", monColor);
        buttonMon.style("color", "black");
        currentDays.push("monday");
        filterByDay();
      }
    });

    buttonTue.mousePressed(function(e) {
      if (buttonTue.elt.style.backgroundColor == tueColor) {
        buttonTue.style("background-color", "black");
        buttonTue.style("color", "white");
        removeFromCurrentDaysArray("tuesday");
      } else {
        buttonTue.style("background-color", tueColor);
        currentDays.push("tuesday");
        filterByDay();
      }
    });

    buttonWed.mousePressed(function(e) {
      if (buttonWed.elt.style.backgroundColor == wedColor) {
        buttonWed.style("background-color", "black");
        buttonWed.style("color", "white");
        removeFromCurrentDaysArray("wednesday");
      } else {
        buttonWed.style("background-color", wedColor);
        currentDays.push("wednesday");
        filterByDay();
      }
    });

    buttonThu.mousePressed(function(e) {
      if (buttonThu.elt.style.backgroundColor == thuColor) {
        buttonThu.style("background-color", "black");
        buttonThu.style("color", "white");
        removeFromCurrentDaysArray("thursday");
      } else {
        buttonThu.style("background-color", thuColor);
        currentDays.push("thursday");
        filterByDay();
      }
    });

    buttonFri.mousePressed(function(e) {
      if (buttonFri.elt.style.backgroundColor == friColor) {
        buttonFri.style("background-color", "black");
        buttonFri.style("color", "white");
        removeFromCurrentDaysArray("friday");
      } else {
        buttonFri.style("background-color", friColor);
        currentDays.push("friday");
        filterByDay();
      }
    });

    buttonSat.mousePressed(function(e) {
      if (buttonSat.elt.style.backgroundColor == satColor) {
        buttonSat.style("background-color", "black");
        buttonSat.style("color", "white");
        removeFromCurrentDaysArray("saturday");
      } else {
        buttonSat.style("background-color", satColor);
        currentDays.push("saturday");
        filterByDay();
      }
    });
  });
}

function processData(data) {
  // For all of the data in the array
  for (let i = 0; i < data.length; i++) {
    // Only for the DUI incident violations
    if (data[i].incident_description == "DUI") {
      // Disregard all of the data that is recorded at 12am (default time of accident)
      var incidentDate = new Date(data[i].incident_datetime);
      if (!(incidentDate.getHours() == 0 && incidentDate.getMinutes() == 0)) {
        // create a new object and populate it with data from the crime data array
        let thisDataLine = new DataLine(
          data[i].day_of_week,
          incidentDate,
          data[i]
        );
        // store the objects in an array
        dataLines.push(thisDataLine);
      }
    }
  }
}

function filterByDay() {
  // if there is more than one item in the currentDays array
  if (currentDays.length > 0) {
    // sort the array based on time of the week
    sortDayOfTheWeekArray();
  }
  // this array will hold the items that match the day of the week with the currentDays array
  filteredLines = [];
  let count = 0;
  // for the days which are seleced by the buttons
  for (let j = 0; j < currentDays.length; j++) {
    // for the dataLines array
    for (let i = 0; i < dataLines.length; i++) {
      // set the variable from the objects day of the week
      let dayFromDataLines = dataLines[i].day.toLowerCase();
      // when the object's DOTW matches the any of the days from the currentDays array
      if (dayFromDataLines == currentDays[j]) {
        // add the add the object to the filtered array
        filteredLines[count] = dataLines[i];
        count++;
      }
    }
  }
  // display all of the filtered lines based on user's selection
  for (let i = 0; i < filteredLines.length; i++) {
    filteredLines[i].show();
  }
}

function sortDayOfTheWeekArray() {
  // in order to prevent data that has longer lines from covering lines that are shorter,
  // sort the currentDays array and display the data from longest lines to shortest
  for (let i = 0; i < currentDays.length; i++) {
    if (currentDays[i] == "sunday") {
      currentDays[i] = 6;
    } else if (currentDays[i] == "monday") {
      currentDays[i] = 5;
    } else if (currentDays[i] == "tuesday") {
      currentDays[i] = 4;
    } else if (currentDays[i] == "wednesday") {
      currentDays[i] = 3;
    } else if (currentDays[i] == "thursday") {
      currentDays[i] = 2;
    } else if (currentDays[i] == "friday") {
      currentDays[i] = 1;
    } else if (currentDays[i] == "saturday") {
      currentDays[i] = 0;
    }
  }
  currentDays = sort(currentDays, currentDays.length);
  for (let i = 0; i < currentDays.length; i++) {
    if (currentDays[i] == 6) {
      currentDays[i] = "sunday";
    } else if (currentDays[i] == 5) {
      currentDays[i] = "monday";
    } else if (currentDays[i] == 4) {
      currentDays[i] = "tuesday";
    } else if (currentDays[i] == 3) {
      currentDays[i] = "wednesday";
    } else if (currentDays[i] == 2) {
      currentDays[i] = "thursday";
    } else if (currentDays[i] == 1) {
      currentDays[i] = "friday";
    } else if (currentDays[i] == 0) {
      currentDays[i] = "saturday";
    }
  }
}

function removeFromCurrentDaysArray(day) {
  // remove the deselected day from the currentDays array
  var dayIndex;
  dayIndex = currentDays.indexOf(day);
  if (dayIndex > -1) {
    currentDays.splice(dayIndex, 1);
  }
  // clear the canvas and redraw the images
  clear();
  image(
    theImage,
    (window.innerWidth - imageSize) / 5.0,
    window.innerHeight / 2 - imageSize / 2,
    imageSize,
    imageSize
  );
  image(
    theImage2,
    (window.innerWidth - imageSize) / 1.1,
    window.innerHeight / 2 - imageSize / 2,
    imageSize,
    imageSize
  );
  // update the visual data to reflect deselection of the day
  if (currentDays.length > 0) {
    for (let i = 0; i < currentDays.length; i++) {
      filterByDay(currentDays[i]);
    }
  }
}

function draw() {
  // refresh the page upon window resize
  window.onresize = function() {
    location.reload();
  };
}

function mouseMoved() {
  // display data details on the hovered item
  var centerX = clockCenterX;
  var centerY = clockCenterY;
  // calculate the position of the mouse in degrees in relationship to the clock
  degrees = Math.atan2(mouseY - centerY, mouseX - centerX);
  degrees = convertToDegrees(degrees);
  // map the degrees to hours
  let mapped = map(degrees, 0, 360, 0, 24);
  let timeHover;
  if (mapped > 18) {
    timeHover = mapped - 18;
  } else {
    timeHover = mapped + 6;
  }
  // if there are more than one day selected
  if (currentDays.length > 0) {
    // for the entire array of the filteres DUI objects
    for (let i = 0; i < filteredLines.length; i++) {
      // calculate the distance between user's hovering and actual data point location on the clock
      if (Math.abs(timeHover - filteredLines[i].time) < 0.02) {
        // if the hovering is close to the specified proximity
        // repaing the panel image
        image(
          theImage2,
          (window.innerWidth - imageSize) / 1.1,
          window.innerHeight / 2 - imageSize / 2,
          imageSize,
          imageSize
        );
        // create Date objects from the filetered DUI objects
        let occurredTime = new Date(filteredLines[i].data.incident_datetime);
        let createdTime = new Date(filteredLines[i].data.created_at);
        let updatedTime = new Date(filteredLines[i].data.updated_at);
        // populate the string with the details from the DUI case
        crimeInfo =
          "Incident / Investigation Report\nCase Number: " +
          filteredLines[i].data.case_number +
          "\nIncident ID: " +
          filteredLines[i].data.incident_id +
          "\nLocation: \n" +
          filteredLines[i].data.address_1 +
          "\n" +
          filteredLines[i].data.city +
          ", " +
          filteredLines[i].data.state +
          " " +
          filteredLines[i].data.zip +
          "\nOccurred: " +
          occurredTime.toString() +
          "\nAdded: " +
          createdTime.toString() +
          "\nUpdated: " +
          updatedTime.toString();
        // reinitialize the text settings
        fill("black");
        noStroke();
        textSize(23);
        // print the data about the DUI to the panel
        text(
          crimeInfo,
          80 + (window.innerWidth - imageSize) / 1.1,
          15 + window.innerHeight / 2 - imageSize / 2,
          imageSize - 180,
          imageSize
        );
      }
    }
    // if there are no days selected
  } else {
    // informe the users what to do by displaying intro message in the panel
    noStroke();
    text(
      crimeInfoOriginal,
      80 + (window.innerWidth - imageSize) / 1.1,
      15 + window.innerHeight / 2 - imageSize / 2,
      imageSize - 180,
      imageSize
    );
  }
}

function convertToDegrees(radians) {
  // converts given radians to degrees
  var toDegrees = radians * (180 / Math.PI);
  if (toDegrees < 0) {
    toDegrees = 360 - Math.abs(toDegrees);
  }
  return toDegrees;
}

class DataLine {
  // the constructor for the DUI object
  constructor(day, date, theData) {
    // stores various information about the DUI object, including full data of the DUI
    this.day = day;
    this.date = date;
    this.length = 30;
    // Convert hour and minute to decimal
    let hour = this.date.getHours();
    let minutes = this.date.getMinutes();
    let fraction = minutes / 60.0;
    this.time = hour + fraction;
    this.radians = this.convertTimeToRadians(this.time);
    this.lineColor = this.setDayColor(this.day);
    this.data = theData;
  }

  show() {
    let theta = this.radians;
    // translation is based on the clock's center point
    let translationX = clockCenterX;
    let translationY = clockCenterY;
    // calculate the x and y coordinates of the inner line point
    let x1 = cos(theta) * radius;
    let y1 = sin(theta) * radius;
    // perform translation to align with the clock
    x1 += translationX;
    y1 += translationY;
    // change the length of the line based on day of the week
    let theCurrentDay = this.day.toLowerCase();
    if (theCurrentDay == "monday") {
      this.length = 40;
    } else if (theCurrentDay == "tuesday") {
      this.length = 50;
    } else if (theCurrentDay == "wednesday") {
      this.length = 60;
    } else if (theCurrentDay == "thursday") {
      this.length = 70;
    } else if (theCurrentDay == "friday") {
      this.length = 80;
    } else if (theCurrentDay == "saturday") {
      this.length = 90;
    }
    // calculate the outter points of the line with modifications to the radius length
    let x2 = cos(theta) * (radius + this.length);
    let y2 = sin(theta) * (radius + this.length);
    x2 += translationX;
    y2 += translationY;
    // set the stroke thickness and color and draw the line
    strokeWeight(5);
    stroke(this.lineColor);
    line(x1, y1, x2, y2);
  }

  convertTimeToRadians(time) {
    // converts time to radians
    let mapped = map(time, 0, 24, Math.PI, 3 * Math.PI);
    let radians;
    if (mapped < Math.PI * 1.5) {
      radians = mapped + Math.PI / 2.0;
    } else {
      radians = mapped - Math.PI * 1.5;
    }
    return radians;
  }

  convertDegreesToHours(degrees) {
    // converts given degrees to hours
    let mapped = map(degrees, 0, 360, 0, 24);
    let time;
    if (mapped > 18) {
      time = mapped - 18;
    } else {
      time = mapped + 6;
    }
    return time;
  }

  setDayColor(dayOfTheWeek) {
    // set the storke color based on the day of the week
    var dayColor;
    dayOfTheWeek = dayOfTheWeek.toLowerCase();
    if (dayOfTheWeek == "sunday") {
      dayColor = sunColor;
    } else if (dayOfTheWeek == "monday") {
      dayColor = monColor;
    } else if (dayOfTheWeek == "tuesday") {
      dayColor = tueColor;
    } else if (dayOfTheWeek == "wednesday") {
      dayColor = wedColor;
    } else if (dayOfTheWeek == "thursday") {
      dayColor = thuColor;
    } else if (dayOfTheWeek == "friday") {
      dayColor = friColor;
    } else if (dayOfTheWeek == "saturday") {
      dayColor = satColor;
    }
    return dayColor;
  }
}
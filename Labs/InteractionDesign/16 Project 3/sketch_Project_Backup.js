// Written By: Pavel Krivopustov
// Date: 02/24/2018
// Description:


var crimeData;

var sundayCrime = [];
var mondayCrime = [];
var tuesdayCrime = [];
var wednesdayCrime = [];
var thursdayCrime = [];
var fridayCrime = [];
var saturdayCrime = [];
var sunColor = "rgb(248, 255, 27)";
var monColor = "rgb(167, 213, 78)";
var tueColor = "rgb(28, 172, 87)";
var wedColor = "rgb(37, 116, 191)";
var thuColor = "rgb(123, 76, 156)";
var friColor = "rgb(235, 75, 51)";
var satColor = "rgb(244, 136, 63)";

var currentDays = [];
var dataLines = [];

var radius = 300;
var count = 0;

function setup() {
  // Create a canvas to fill the entire browser
  createCanvas(window.innerWidth, window.innerHeight);
  test();
  // buttonSun.mousePressed();
  //noLoop();
  ///ellipse(window.innerWidth / 2, window.innerHeight / 2, window.innerHeight * .8, window.innerHeight * .8);
}

function preload() {
  $.ajax({
    url: "https://moto.data.socrata.com/resource/4h35-4mtu.json",
    type: "GET",
    data: {
      $limit: 30000,
      $$app_token: "DuaakxC45Z6cqANMR8SNOy4Bf"
    }
  }).done(function(data) {
    //alert("Retrieved " + data.length + " records from the dataset!");
    //console.log(crimeData);
    crimeData = data;
    processData(crimeData);
    // add buttons to the top of the screen
    buttonSun = createButton("SUNDAY");
    buttonSun.position(width / 8, 65);
    buttonSun.style("background-color", "black");
    buttonSun.style("color", "white");

    buttonMon = createButton("MONDAY");
    buttonMon.position(2 * (width / 8), 65);
    buttonMon.style("background-color", "black");
    buttonMon.style("color", "white");

    buttonTue = createButton("TUESDAY");
    buttonTue.position(3 * (width / 8), 65);
    buttonTue.style("background-color", "black");
    buttonTue.style("color", "white");

    buttonWed = createButton("WEDNESDAY");
    buttonWed.position(4 * (width / 8), 65);
    buttonWed.style("background-color", "black");
    buttonWed.style("color", "white");

    buttonThu = createButton("THURSDAY");
    buttonThu.position(5 * (width / 8), 65);
    buttonThu.style("background-color", "black");
    buttonThu.style("color", "white");

    buttonFri = createButton("FRIDAY");
    buttonFri.position(6 * (width / 8), 65);
    buttonFri.style("background-color", "black");
    buttonFri.style("color", "white");

    buttonSat = createButton("SATURDAY");
    buttonSat.position(7 * (width / 8), 65);
    buttonSat.style("background-color", "black");
    buttonSat.style("color", "white");

    // after the button is pressed, change the color and draw the elements
    buttonSun.mousePressed(function(e) {
      //console.log(e);
      // change the color of the button depending if it was clicked or not
      if (buttonSun.elt.style.backgroundColor == sunColor) {
        buttonSun.style("background-color", "black");
        buttonSun.style("color", "white");
      } else {
        buttonSun.style("background-color", sunColor);
        buttonSun.style("color", "black");
      }
      displayCrimeData(sundayCrime);
    });

    buttonMon.mousePressed(function(e) {
      // change the color of the button depending if it was clicked or not
      if (buttonMon.elt.style.backgroundColor == monColor) {
        buttonMon.style("background-color", "black");
        buttonMon.style("color", "white");
      } else {
        buttonMon.style("background-color", monColor);
        buttonMon.style("color", "black");
      }
      displayCrimeData(mondayCrime);
    });

    buttonTue.mousePressed(function(e) {
      // change the color of the button depending if it was clicked or not
      if (buttonTue.elt.style.backgroundColor == tueColor) {
        buttonTue.style("background-color", "black");
        buttonTue.style("color", "white");
      } else {
        buttonTue.style("background-color", tueColor);
      }
      displayCrimeData(tuesdayCrime);
    });

    buttonWed.mousePressed(function(e) {
      // change the color of the button depending if it was clicked or not
      if (buttonWed.elt.style.backgroundColor == wedColor) {
        buttonWed.style("background-color", "black");
        buttonWed.style("color", "white");
      } else {
        buttonWed.style("background-color", wedColor);
        buttonWed.style("color", "white");
      }
      displayCrimeData(wednesdayCrime);
    });

    buttonThu.mousePressed(function(e) {
      // change the color of the button depending if it was clicked or not
      if (buttonThu.elt.style.backgroundColor == thuColor) {
        buttonThu.style("background-color", "black");
        buttonThu.style("color", "white");
      } else {
        buttonThu.style("background-color", thuColor);
      }
      displayCrimeData(thursdayCrime);
    });

    buttonFri.mousePressed(function(e) {
      // change the color of the button depending if it was clicked or not
      if (buttonFri.elt.style.backgroundColor == friColor) {
        buttonFri.style("background-color", "black");
        buttonFri.style("color", "white");
      } else {
        buttonFri.style("background-color", friColor);
      }
      displayCrimeData(fridayCrime);
    });

    buttonSat.mousePressed(function(e) {
      // change the color of the button depending if it was clicked or not
      if (buttonSat.elt.style.backgroundColor == satColor) {
        buttonSat.style("background-color", "black");
        buttonSat.style("color", "white");
      } else {
        buttonSat.style("background-color", satColor);
      }
      displayCrimeData(saturdayCrime);
    });

  });
}

function processData(data) {
  // For all of the data in the array
  for (let i = 0; i < data.length; i++) {
    // Only for the DUI incident violation
    if (data[i].incident_description == "DUI") {
      // Disregard all of the data that is recorded at 12am (default time of accident)
      var incidentDate = new Date(data[i].incident_datetime);
      if (!(incidentDate.getHours() == 0 && incidentDate.getMinutes() == 0)) {
        // Add the data to proper day of the week arrays


        let thisDataLine = new DataLine(data[i].day_of_week, incidentDate);
        dataLines.push(thisDataLine);

        // if (data[i].day_of_week == "Sunday") {
        //   sundayCrime.push(data[i]);
        // } else if (data[i].day_of_week == "Monday") {
        //   mondayCrime.push(data[i]);
        // } else if (data[i].day_of_week == "Tuesday") {
        //   tuesdayCrime.push(data[i]);
        // } else if (data[i].day_of_week == "Wednesday") {
        //   wednesdayCrime.push(data[i]);
        // } else if (data[i].day_of_week == "Thursday") {
        //   thursdayCrime.push(data[i]);
        // } else if (data[i].day_of_week == "Friday") {
        //   fridayCrime.push(data[i]);
        // } else {
        //   saturdayCrime.push(data[i]);
        // }
      }
    }
  }
}
function test() {

    testLines = [];

    for (let i = 0; i < 24; i++) {

      var date1 = new Date('December 17, 1995 0' + i + ':00:00');
      let thisDataLine = new DataLine("Monday", date1);
      testLines[i] = thisDataLine;

    }

    for (let i = 0; i < testLines.length; i++) {
      testLines[i].show();
    }

}
function draw() {
  // for (let i = 0; i < dataLines.length; i++) {

  //   let thisDataLine = dataLines[i];
  //   if (thisDataLine.day == currentDay) {
  //     thisDataLine.show();   
  //   }
  // }
}

function displayCrimeData(arrayData) {
  print(arrayData);
  print(dataLines);
}

class DataLine {

  constructor(day, date) {
    this.day = day;
    this.date = date;
    this.length = 50;
    this.radians = 0;


  }

  show() {

    // Convert hour and minute to decimal
    let hour = this.date.getHours();
    let minutes = this.date.getMinutes();
    let fraction = minutes / 60.0;
    let time = hour + fraction;

    let radians = this.convertTimeToRadians(time);

    // draw the line segment

    let translationX = width / 2.0;
    let translationY = height / 2.0;

    let x1 = cos(radians) * radius;
    let y1 = sin(radians) * radius;

    x1 += translationX;
    y1 += translationY;

    let x2 = cos(radians) * (radius + this.length);
    let y2 = sin(radians) * (radius + this.length);

    x2 += translationX;
    y2 += translationY;

    strokeWeight(5);

    line(x1, y1, x2, y2);
    textSize(27);
    text(count, x2, y2);
    count++;
  }


  convertTimeToRadians(time) {
      let mapped = map(time, 0, 24, Math.PI, 3 * Math.PI);
      let radians;
      if (mapped < Math.PI * 1.5) {
        radians = mapped + Math.PI / 2.0;
      } else {
        radians = mapped - Math.PI * 1.5;
      }

      return radians;
  }

  convertDegreesToHours(degrees) {
      let mapped = map(degrees, 0, 360, 0, 24);
      let time;
      if (mapped > 18) {
        time = mapped - 18;
      } else {
        time = mapped + 6;    
      }
    
      return time;
  }

  hoverOver(x, y) {

    return false;
  }


}
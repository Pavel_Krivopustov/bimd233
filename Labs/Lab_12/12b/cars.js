var cars = [
  ["Austin", "Allegro", 1973, 1999],  
  ["Lada", "VAZ-2107", 1983, 3700],
  ["Lexus", "IS", 2001, 7500],
  ["MINI", "Countryman", 2010, 12999],
  ["Reliant", "Robin", 1981, 3000]
];

for(var i=1; i < cars.length; i++){
	var makeVar = "make" + i;
	var modelVar = "model" + i;
	var yearVar = "year" + i;
	var priceVar = "price" + i;
	
	document.getElementById(makeVar).innerHTML = 
	cars[(i-1)][0];
	document.getElementById(modelVar).innerHTML = 
	cars[(i-1)][1];
	document.getElementById(yearVar).innerHTML = 
	cars[(i-1)][2];
	document.getElementById(priceVar).innerHTML = 
	"$" + cars[(i-1)][3].toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');;
}






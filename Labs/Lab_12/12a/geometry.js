var radius1 = 25;
var radius2 = 50;
var radius3 = 100;

function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  var geometries = [area, circumference, diameter];
  return geometries;
}

document.getElementById("cirArea1").innerHTML = 
	"Area: " + calcCircleGeometries(radius1)[0].toFixed(2);
document.getElementById("cirCircumference1").innerHTML =
  "Circumference: " + calcCircleGeometries(radius1)[1].toFixed(2);
document.getElementById("cirDiameter1").innerHTML =
  "Diameter: " + calcCircleGeometries(radius1)[2].toFixed(2);

document.getElementById("cirArea2").innerHTML =
  "Area: " + calcCircleGeometries(radius2)[0].toFixed(2);
document.getElementById("cirCircumference2").innerHTML =
  "Circumference: " + calcCircleGeometries(radius2)[1].toFixed(2);
document.getElementById("cirDiameter2").innerHTML =
  "Diameter: " + calcCircleGeometries(radius2)[2].toFixed(2);

document.getElementById("cirArea3").innerHTML =
  "Area: " + calcCircleGeometries(radius3)[0].toFixed(2);
document.getElementById("cirCircumference3").innerHTML =
  "Circumference: " + calcCircleGeometries(radius3)[1].toFixed(2);
document.getElementById("cirDiameter3").innerHTML =
  "Diameter: " + calcCircleGeometries(radius3)[2].toFixed(2);
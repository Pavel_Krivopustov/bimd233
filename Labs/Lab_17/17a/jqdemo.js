$(document).ready(function () {
    // add a 5px red dashed boarder around the panel div
    // provide a 10 pixel padding around all of the divs
    $('.panel').css('border', '5px dashed red');
    $('.panel').css('padding', '10px');
    // set all div's padding to 3px
    $('div').css('padding', '3px');
    // set all divs in the panel to background gray
    $('div.cat,div.dog').css('background', 'gray');
    // set all divs of class cat to green
    $('.cat').css('background', 'green');
    // set all divs of class dog to red
    $('.dog').css('background', 'red');
    // set the id of lab to a 1px solid yellow border
    $('#lab').css('border', '1px solid yellow');
    // set the last div with class dog to background yellow
    $('.dog').eq(1).css('background', 'yellow');
    // set the calico cat's width to 50%, 
    $('.cat#calico').css('width','50%');
    // background to green and color to white 
    $('.cat#calico').css('color','white');
  
    // to make the result look like in the slides, 
    // additional selections were made
    $('.cat').eq(0).css('background', 'gray');
    $('div.dog#lab').css('background', 'gray');
    $('.cat').eq(0).css('color','green');
    $('.dog').css('color','red');
    $('.panel').css('font-weight','Bold');
});
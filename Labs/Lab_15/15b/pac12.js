/*<!-- Pavel Krivopustov, 11/27/2017, IMD 233 -->*/

function UniversityInfo(
  imgLink,
  rank,
  name,
  winConf,
  lossConf,
  totalWin,
  totalLoss,
  lastResult,
  lastGame
) {
  this.imgLink = imgLink;
  this.rank = rank;
  this.name = name;
  this.winConf = winConf;
  this.lossConf = lossConf;
  this.totalWin = totalWin;
  this.totalLoss = totalLoss;
  this.lastResult = lastResult;
  this.lastGame = lastGame;
}

var UW = new UniversityInfo(
  "http://x.pac-12.com/sites/default/files/styles/thumbnail/public/washington-logo_0__1438812441.png?itok=gDQCuMJ2",
  6,
  "Washington",
  7,
  1,
  10,
  1,
  "W",
  "44-18 ASU"
);
var WSU = new UniversityInfo(
  "http://x.pac-12.com/sites/default/files/styles/thumbnail/public/washington-state-logo__1438812470.png?itok=q0ynricm",
  22,
  "Washington State",
  7,
  1,
  8,
  3,
  "L",
  "24-38 COLO"
);
var SU = new UniversityInfo(
  "http://x.pac-12.com/sites/default/files/styles/thumbnail/public/Stanford_Logo_New_LG__1438812239.png?itok=3CxB0utl",
  24,
  "Stanford",
  6,
  3,
  8,
  3,
  "W",
  "45-31 CAL"
);
var CA = new UniversityInfo(
  "http://x.pac-12.com/sites/default/files/styles/thumbnail/public/cal_yellow_lg_2017.png?itok=2nn6ONRS",
  -1,
  "California",
  2,
  6,
  4,
  7,
  "L",
  "31-45 STAN"
);
var OU = new UniversityInfo(
  "http://x.pac-12.com/sites/default/files/styles/thumbnail/public/oregon-g-logo__1438812094.png?itok=WvbZePjJ",
  -1,
  "Oregon",
  2,
  6,
  4,
  7,
  "L",
  "30-28 UTAH"
);
var OSU = new UniversityInfo(
  "http://x.pac-12.com/sites/default/files/styles/thumbnail/public/oregon-state-logo_0__1438812147.png?itok=4KiO01vm",
  -1,
  "Oregon State",
  2,
  6,
  3,
  8,
  "W",
  "42-17 ARIZ"
);

var teams = [UW, WSU, SU, CA, OU, OSU];

var tableRef = document.getElementById("table-game-results");
for (var i = 0; i < teams.length; i++) {
  var j = 0;
  var newRow = tableRef.insertRow(tableRef.rows.length);

  var newCell = newRow.insertCell(j);
  newCell.innerHTML = '<img height=30px; src="' + teams[i].imgLink + '">';
  j++;

  var newCell = newRow.insertCell(j);
  if (teams[i].rank !== -1) {
    newCell.innerHTML =
      "<b>" +
      teams[i].rank +
      "</b>" +
      " " +
      "<span>" +
      teams[i].name +
      "</span>";
  } else {
    newCell.innerHTML = "<span>" + teams[i].name + "</span>";
  }
  j++;

  var newCell = newRow.insertCell(j);
  newCell.textContent = teams[i].winConf + "-" + teams[i].lossConf;
  j++;

  var newCell = newRow.insertCell(j);
  newCell.textContent = teams[i].totalWin + "-" + teams[i].totalLoss;
  j++;

  var newCell = newRow.insertCell(j);

  var result = '<b class="special-letters">' + teams[i].lastResult + "</b> ";
  var game = "<span>" + teams[i].lastGame + "</span>";
  newCell.innerHTML = result + game;

  if (teams[i].name == "Oregon State") {
    tableRef.getElementsByTagName("tr")[0].style.backgroundColor =
      "rgba(75, 46, 131, .6)";
    tableRef.getElementsByTagName("tr")[1].style.backgroundColor =
      "rgba(152, 30, 50, .6)";
    tableRef.getElementsByTagName("tr")[2].style.backgroundColor =
      "rgba(153, 0, 0, .6)";
    tableRef.getElementsByTagName("tr")[3].style.backgroundColor =
      "rgba(8, 31, 63, .6)";
    tableRef.getElementsByTagName("tr")[4].style.backgroundColor =
      "rgba(21, 71, 51, .6)";
    tableRef.getElementsByTagName("tr")[5].style.backgroundColor =
      "rgba(37, 37, 37, .6)";
  }
}

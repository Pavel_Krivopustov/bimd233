/*<!-- Pavel Krivopustov, 11/22/2017, IMD 233 -->*/

var companies = new Array;

var company = [
  {
    companyName: "Microsoft",
    marketCap: "$381.7 B",
    sales: "$86.8 B",
    profits: "$22.1 B",
    employees: "~128,000"
  },
  {
    companyName: "Symetra Financial",
    marketCap: "$2.7 B",
    sales: "$2.2 B",
    profits: "$254.4 M",
    employees: "~1,400"
  },
  {
    companyName: "Micron Technology",
    marketCap: "$37.6 B",
    sales: "$16.4 B",
    profits: "$3.0 B",
    employees: "~30,400"
  },
  {
    companyName: "F5 Networks",
    marketCap: "$9.5 B",
    sales: "$1.7 B",
    profits: "$311.2 M",
    employees: "~3,834"
  },
  {
    companyName: "Expedia",
    marketCap: "$10.8 B",
    sales: "$5.8 B",
    profits: "$398.1 M",
    employees: "~18,210"
  },
  {
    companyName: "Nautilus",
    marketCap: "$476 M",
    sales: "$274.4 M",
    profits: "$18.8 M",
    employees: "~340"
  },
  {
    companyName: "Heritage Financial",
    marketCap: "$531 M",
    sales: "$137.6 M",
    profits: "$21 M",
    employees: "~748"
  },
  {
    companyName: "Cascade Microtech",
    marketCap: "$239 M",
    sales: "$136 M",
    profits: "$9.9 M",
    employees: "~449"
  },
  {
    companyName: "Nike",
    marketCap: "$83.1 B",
    sales: "$27.8 B",
    profits: "$2.7 B",
    employees: "~56,500"
  },
  {
    companyName: "Alaska Air Group",
    marketCap: "$7.9 B",
    sales: "$5.4 B",
    profits: "$605 M",
    employees: "~13,952"
  }
];

 for (i = 0; i < 10; i++) {
    companies.push(company[i]);
  }

function loadInfo(item, index) {
  var el = document.getElementById("stocksTable");

  el.innerHTML +=
    "<tr><td> <sty>" +
    companies[index]["companyName"] +
    "</sty> </td><td> <sty>" +
    companies[index]["marketCap"] +
    "</sty> </td><td> <sty>" +
    companies[index]["sales"] +
    "</sty> </td><td> <sty>" +
    companies[index]["profits"] +
    "</sty> </td><td> <sty>" +
    companies[index]["employees"] +
    "</sty> </td></tr>";
}
// Assigned modifications were made by Pavel Krivopustov on 12/04/2017

// Shorthand for $( document ).ready()
$(function() {
  // weather update button click
  $('button').on('click', function(e) {
    $('ul li').each(function() {
      console.log("this:" + this);
      $(this).remove();
    });
    $.ajax({
      url: "http://api.wunderground.com/api/4e7a4efc38a07f71/geolookup/conditions/q/WA/Bothell.json",
      dataType: "jsonp",
      success: function(parsed_json) {
        var city = parsed_json['location']['city'];
        var state = parsed_json['location']['state'];
        var temp_f = parsed_json['current_observation']['temp_f'];
        var rh = parsed_json['current_observation']['relative_humidity'];
        var str = "<li> Location : " + city + ", " + state + "</li>";
		var wthr = parsed_json['current_observation']['weather'];
		var wnd = parsed_json['current_observation']['wind_string'];
		var time = parsed_json['current_observation']['observation_time_rfc822'];
		var observedTime = new Date(time);
		
		var hour = observedTime.getHours();
        // Update list items with data from above...
         $("ul").empty();
		 $('ul').append(str);
         $('ul li:last').attr('class', 'list-group-item');
         str = "<li> Current Temperature is: " + temp_f + "</li>";
         $('ul').append(str);
         $('ul li:last').attr('class', 'list-group-item');
         str = "<li> Relative Humidity is: " + rh + "</li>";
         $('ul').append(str);
         $('ul li:last').attr('class', 'list-group-item');
		 var iconWeather;
         // Get the proper weather icon based on time of day
		 if(hour > 6 && hour < 20) // Get day icon between 6am and 8pm
		 {
			 iconWeather = wthr.replace(/\s+/g, '').toLowerCase();
		 } else {
			 iconWeather = "nt_" + wthr.replace(/\s+/g, '').toLowerCase();
		 }
		 
		 
		 str = "<li> Current Weather is: " + wthr + " " + "<img src=\"https://icons.wxug.com/i/c/k/" + iconWeather + ".gif\" height=\"32\" width=\"32\">" + "</li>";
		 $('ul').append(str);
         $('ul li:last').attr('class', 'list-group-item');
		 str = "<li> Current Wind is: " + wnd + "</li>";
		 $('ul').append(str);
         $('ul li:last').attr('class', 'list-group-item');
		 
        console.log("Current temperature in " + city + " is: " + temp_f);
      }
    });
  });
});
